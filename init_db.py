import argparse
import json
import sqlite3
import sys
import pathlib

from datetime import datetime
from scara_web import crypto


DB_PATH = pathlib.Path(__file__).parent / 'database.db'
CANDIDATES_NUM = 3

def setup_db(db_path=DB_PATH):
    conn = sqlite3.connect(db_path)
    create_tables(conn)
    conn.commit()

def create_tables(conn):
    cur = conn.cursor()
    #Tabel DHT
    dht = """
        CREATE TABLE IF NOT EXISTS dht (
            id          INTEGER         PRIMARY KEY,
            timestamp   TIMESTAMP       NOT NULL,
            node_id     VARCHAR(182)    UNIQUE NOT NULL,
            signature   VARCHAR(128)    NOT NULL
        )
    """
    cur.execute(dht)

    #Tabel Kandidat
    #Candidates Table
    cand = ''.join('cand_{} INTEGER NOT NULL,'.format(i) for i in range(CANDIDATES_NUM))
    candidates = """
        CREATE TABLE IF NOT EXISTS candidates ({}
        block_id INTEGER UNIQUE NOT NULL)""".format(cand)
    cur.execute(candidates)

    #Tabel Nodes
    nodes = """
        CREATE TABLE IF NOT EXISTS nodes (
            id      INTEGER         PRIMARY KEY,
            name    TEXT            UNIQUE NOT NULL,
            pub_key VARCHAR(182)    UNIQUE NOT NULL,
            ip_addr VARCHAR(15),
            port_addr INTEGER
        )
    """
    cur.execute(nodes)

def delete_all_tables(conn):

    cur = conn.cursor()
    sql = "DROP TABLE IF EXISTS blocks"
    cur.execute(sql)
    sql = "DROP TABLE IF EXISTS candidates"
    cur.execute(sql)
    sql = "DROP TABLE IF EXISTS nodes"
    cur.execute(sql)

def create_test_nodes(conn, num_of_nodes: int):
    cur = conn.cursor()
    sql = "INSERT INTO nodes (name, pub_key) VALUES (?, ?)"
    data = []
    for i in range(num_of_nodes):
        name = 'TPS {}'.format(i+1)
        key = crypto.gen_ecc_pair_key()
        cur.execute(sql, (name, key[1].hex()))
        data.append({
            'name': name,
            'private_key': key[0].hex(),
            'public_key': key[1].hex()
        })
    with open('tps_keys.json', 'w') as outfile:
        json.dump(data, outfile)

def main():
    parser = argparse.ArgumentParser( description="Scara DB. Create database.db file if no options \specified")
    parser.add_argument(
        '--setupdb', '-stdb', action='store_true', help="Create database and all the tables for scara web app"
    )
    parser.add_argument(
        '--dbname', '-db', type=str, default=DB_PATH, help="Database name"
    )
    parser.add_argument(
        '--cnodes', '-cn', type=int, help="Create some test nodes"
    )
    parser.add_argument(
        '--drop', '-d', action='store_true', help="Delete All Tables"
    )
    args = parser.parse_args()
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    else:
        conn = sqlite3.connect(args.dbname)
        if args.setupdb:
            setup_db(args.dbname)
        if args.drop:
            try:
                delete_all_tables(conn)
                conn.commit()
            except sqlite3.Error as e:
                conn.rollback()
                print('Cannot delete all tables,', e)
        if args.cnodes:
            try:
                create_test_nodes(conn, args.cnodes)
                conn.commit()
            except sqlite3.Error as e:
                conn.rollback()
                print('Cannot generate test nodes,', e)

if __name__ == "__main__":
    main()
