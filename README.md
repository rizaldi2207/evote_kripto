# SCARA (Sistem Pencatatan Pemungutan Suara)

Scara merupakan sebuah aplikasi pencatatan hasil pemungutan suara berbasis kriptografi.

## Setup Database
Sebelum menjalankan aplikasi, perlu dilakukan setup database dengan menggunakan program pada file `init_db.py`.

```
$ python init_db.py --setupdb --dbname database.db --cnodes (jumlah node)
```

Perintah diatas akan membuat file `database.db` pada root project.


## Menjalankan Aplikasi
File untuk menjalankan aplikasi adalah file `main.py`. Aplikasi/program ini dijalankan dengan cara:

```
python main.py
```

Maka akan tampil IP dan Port yang digunakan untuk menjalankan Web Application sesuai dengan yang diatur pada main.py