import sqlite3

from const import NUM_OF_CANDIDATE, DB_DIR
from holochain import validasi_data

async def db_setup(app):
    db_conn = sqlite3.connect(DB_DIR)
    
    async def db_close(app):
        db_conn.close()
    
    app.on_cleanup.append(db_close)
    
    app['db_conn'] = db_conn
    app['num_of_candidate'] = NUM_OF_CANDIDATE
    # return db_conn

async def create_candidate(conn, candidate_vote):
    cand = candidate_columns()
    sql = """
        INSERT INTO candidates ({}, block_id)
        VALUES ({} ?)
    """.format(cand, '?, ' * NUM_OF_CANDIDATE)
    cur = conn.cursor()
    cur.execute(sql, candidate_vote)
    return cur.lastrowid

async def create_dht(conn, block_data, candidate_vote):
    sql = """
        INSERT INTO dht (
            timestamp, node_id, signature
        ) VALUES (?,?,?)
    """
    cur = conn.cursor()
    cur.execute(sql, block_data)
    _ = await create_candidate(conn, candidate_vote + (cur.lastrowid, ))

async def check_key(conn, block_data) -> bool :
    sql = """
        SELECT node_id FROM dht
    """
    cur = conn.cursor()
    cur.execute(sql)
    result = cur.fetchall()

    is_data_valid = validasi_data(block_data)
    node_id = block_data[2]
    if is_data_valid:
        for row in result:
            for i in range(len(row)):
                if row[i] == node_id:
                    return False
                else:
                    return True
    else:
        print("Tidak bisa memasukkan data")

async def select_latest_block(conn):
    sql = """
        SELECT node_id from dht
    """
    cur = conn.cursor()
    cur.execute(sql)
    return cur.fetchone()

async def count_candidate_votes(conn):
    cand = candidate_columns('SUM(', ')')
    sql = "SELECT {} FROM candidates".format(cand)
    cur = conn.cursor()
    cur.execute(sql)
    result = cur.fetchone()
    return result

def candidate_columns(prefix: str='', suffix: str='') -> str:
    cand = ','.join('{}cand_{}{}'.format(prefix, i, suffix) for i in range(NUM_OF_CANDIDATE))
    return cand

async def select_dht(conn):
    sql = "SELECT * FROM dht"
    cur = conn.cursor()
    cur.execute(sql)
    results = cur.fetchall()
    return results

