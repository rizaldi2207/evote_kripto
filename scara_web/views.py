import sqlite3
import binascii
from flask import render_template
from datetime import datetime
from scara_holochain import holochain, crypto, net
from .database import (select_nodes, count_candidate_votes, create_dht)



def index(request):
    return render_template('index.html')

def contact(request):
    return render_template('contact.html')

def add_dht(request):
    conn = request.app['db_conn']
    data = request.json()

    nodes = select_nodes(conn)

    candidates = {
            int(id): votes for id, votes in data['candidates'].items()
    }
    cand_votes = tuple([votes for id, votes in data['candidates'].items()])

    private_key = data['private_key']
    timestamp = datetime.now()

    try:
        node_id = crypto.get_public_key_ecc(private_key)
        signature = holochain.sign_votes(private_key, node_id, candidates)
        block_data =(timestamp, node_id, signature)

        _ = create_dht(conn, block_data, cand_votes)
        conn.commit()

    except sqlite3.IntegrityError:
        conn.rollback()
        msg_type, msg_text = 'error', 'You have entered data'

    except (binascii.Error, ValueError):
        conn.rollback()
        msg_type, msg_text = 'error', 'Wrong private key'
    except Exception:
        conn.rollback()
        msg_type, msg_text = 'error', 'Something went wrong'

    else:
        msg_type = 'success'
        msg_text = 'The block has been submitted successfully'

    return {'type': msg_type, 'msg': msg_text}