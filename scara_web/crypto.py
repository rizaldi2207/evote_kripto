from Crypto.Hash import SHA256
from Crypto.PublicKey import ECC, RSA
from Crypto.Signature import DSS, pss, pkcs1_15

import binascii

def load_private_key(path):

    return ECC.import_key(open(path).read())

def generate_pair_key(path):

    key = ECC.generate(curve='P-256')
    f = open('keys/myprivatekey.pem','wt')
    f.write(key.export_key(format='PEM'))
    f.close()
    f = open('keys/mypublickey.pem','wt')
    f.write(key.public_key().export_key(format='PEM'))
    f.close()

def gen_ecc_pair_key():
    """ Return generated ecc key, private key and public key, in
    bytes format
    """
    key = ECC.generate(curve='P-256')
    return key.export_key(format='DER'), \
        key.public_key().export_key(format='DER')



def sign_dss(private_key, data):
    """ Sign a data using ECC key with Digital Signature Algorithm
    (DSA or ECDSA)
    Parameters:
    private_key (str hex) : ECC private key
    data (byte) : Data that will be signed
    Returns:
    signature (byte hex)
    """
    key = ECC.import_key(binascii.unhexlify(private_key))
    h = SHA256.new(data)
    return DSS.new(key, 'fips-186-3').sign(h)

def verify_signature_dss(public_key, signature, data):
    key = ECC.import_key(binascii.unhexlify(public_key))
    h = SHA256.new(data)
    verifier = DSS.new(key, 'fips-186-3')
    return verifier.verify(h, binascii.unhexlify(signature))

def get_public_key_ecc(private_key):
    public_key = ECC.import_key(binascii.unhexlify(private_key)).public_key()
    return public_key.export_key(format='DER').hex()

def test_dss(signed_msg, verified_msg):
    key = gen_ecc_pair_key()
    signature = sign_dss(key[0].hex(), signed_msg.encode('utf-8'))
    try:
        verify_signature_dss(
            key[1].hex(), signature.hex(), verified_msg.encode('utf-8')
        )
        print('data valid')
    except ValueError:
        print('data tidak valid')