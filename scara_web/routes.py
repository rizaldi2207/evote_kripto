from .views import index, contact, add_dht
from .const import STATIC_DIR

def setup_routes(app):
    app.router.add_get('/', index, name = 'index')
    app.router.add_get('/contact', contact, name = 'contact')
    app.router.add_post('/data', add_dht, name='add_data')

    app.router.add_static('/static', path=str(STATIC_DIR),name='static')