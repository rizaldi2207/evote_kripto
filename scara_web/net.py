import asyncio as aio
import json
import sqlite3
from flask import jsonify
from holochain import validasi_data
from datetime import datetime
from typing import Dict, List, Tuple
from database import create_dht

NodeAddr = Tuple[str, int]
Block = Tuple[float, str, str, tuple]
Hasil = tuple

async def broadcast_hasil(address: NodeAddr, data: Block):
    (host, port) = address
    try:
        reader, writer = await aio.open_connection(host, port)
    except Exception:
        print("Gagal menghubungkan ke {}: {}".format(host, port))
        return
    data_hash = json.dumps({
        'timestamp': data[0].timestamp(),
        'node_id':data[1],
        'signature':data[2],
        'candidates':data[3]
    })
    writer.write(data_hash.encode() + b'\n')
    writer.close()

async def broadcast_toall(addresses: List[NodeAddr], data: Block):
    for address in addresses:
        await broadcast_hasil(address, data)

class ListeningPeer:
    def __init__(self, conn, port: int):
        self._db = conn
        self._port = port
        self._server = None

    async def start(self):
        self._server = await aio.start_server(self._accept, 8888)
        
        addrs = ', '.join(str(sock.getsockname()) for sock in self._server.sockets)
        print(f'Serving on {addrs}')
        async with self._server:
            await self._server.serve_forever()
    
    async def _accept(self, reader: aio.StreamReader, writer: aio.StreamWriter):
        addr = writer.get_extra_info('peername')
        data = await reader.readline()
        data_dict = json.loads(data.decode())

        await self._save_block(data_dict)
        
        print("Received data from %r" % (addr))

        writer.close()
    async def _save_block(self, data: Dict[str, any], addr):
        data['candidates']= {
            int(id): votes for id, votes in data['candidates'].items()
        }
        cand_votes = tuple([votes for id, votes in data['candidates'].items()])
        block_data = (
            data['timestamp'], data['node_id'], data['signature']
        )

        is_data_valid = validasi_data(block_data)
        if is_data_valid:
            try:
                block_data = (
                    (str(datetime.fromtimestamp(data['timestamp']),)) + block_data[1:]
                    )
                _ = await create_dht(self._db, block_data, cand_votes)
                    
                await self._db.commit()
                
            except sqlite3.IntegrityError: 
                await self._db.rollback()
                    
        else:
            print("Failed to save data")