import hashlib
import json
from Crypto.Hash import SHA256
from Crypto.PublicKey import ECC
from Crypto.Signature import DSS
from datetime import datetime
from crypto import get_public_key_ecc, sign_dss, verify_signature_dss

from typing import Dict, Tuple, List

Blockchain = List[tuple]

def calculate_hash(timestamp: float, node_id: str, signature: str) -> str:
    data = {
        'timestamp': timestamp,
        'node_id': node_id,
        'signature': signature
    }
    data_bytes = json.dumps(data, sort_keys=True).encode()
    return data_bytes

def data_vote(node_id: str, candidate: str) -> str:
    data = {
        'node_id': node_id,
        'candidates': candidate
    }
    return json.dumps(data, sort_keys=True).encode()

def sign_votes(private_key: str, node_id: str, candidates: Dict[int, int]) -> str:
    data_bytes = data_vote(node_id, candidates)
    return sign_dss(private_key, data_bytes).hex()

def verify_signature(public_key: str, signature: str, candidates: Dict[int, int]) -> bool:
    data_bytes = data_vote(public_key, candidates)
    try:
        verify_signature_dss(public_key, signature, data_bytes)
        return True
    except (ValueError, TypeError):
        return False

def validasi_data(data_hash: tuple) -> bool:
    return verify_signature(data_hash[0], data_hash[1], data_hash[2])

