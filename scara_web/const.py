import pathlib

PROJECT_DIR = pathlib.Path(__file__).parent.parent
STATIC_DIR = pathlib.Path(__file__).parent / 'static'
TEMPLATE_DIR = pathlib.Path(__file__).parent / 'templates'

DB_DIR = PROJECT_DIR / 'database.db'
NUM_OF_CANDIDATE = 3