$(document).ready(function() {
    const HostURL = 'http://192.168.1.35:5000';
    
    setInterval(getVotesAggregate, 1000);
    
    addBlock()

    function getVotesAggregate() {
        return fetch(HostURL + '/vote', {
            method: 'GET',
            mode: 'cors'
        })
        .then(response => response.json())
        .then(data => showVotesAggregate(data))
    }

    function calculateVotesPercentage(votesArr) {
        let sum = 0;
        for (let cand of votesArr) {
            sum += parseInt(cand);
        }
        
        let percent = [];
        for (let cand of votesArr) {
            percent.push((parseFloat(cand)/sum * 100).toFixed(2));
        }
        return percent;
    }

    function showVotesAggregate(candidates) {
        let voteInPercentage = calculateVotesPercentage(candidates);

        document.getElementById('votecandidate1').innerHTML = voteInPercentage[0] + ' %';
        document.getElementById('votecandidate2').innerHTML = voteInPercentage[1] + ' %';
        document.getElementById('votecandidate3').innerHTML = voteInPercentage[2] + ' %';
        
    }


    function addBlock() {
        let addBlockForm = document.forms['add_dht'];
        let candidates1 = addBlockForm.elements['calon1']
        let candidates2 = addBlockForm.elements['calon2']
        let candidates3 = addBlockForm.elements['calon3']
        let private_key = addBlockForm.elements['PrivateKey'];

        let submitButton = addBlockForm.querySelector('button[type=submit]');

        submitButton.addEventListener('click', function(e) {
            e.preventDefault();
            
            if (private_key.value === '') {
                showMessage({'type': 'error', 'msg': 'Fields cannot be empty'});
                return;
            }
            
            if (candidates1.value === '') {
                showMessage({'type': 'error', 'msg': 'Fields cannot be empty'});
                return;
            }
            
            if (candidates2.value === '') {
                showMessage({'type': 'error', 'msg': 'Fields cannot be empty'});
                return;
            }

            if (candidates3.value === '') {
                showMessage({'type': 'error', 'msg': 'Fields cannot be empty'});
                return;
            }

            votes = {}
            votes[0] = candidates1.value
            votes[1] = candidates2.value
            votes[2] = candidates3.value
            
            

            let data = {
                'candidates': votes,  
                'private_key': private_key.value
            }

            fetch(HostURL + '/data', {
                method: 'POST',
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(message => showMessage(message))
        });
    }
    
    function showMessage(message) {
        let blockMessageContainer = document.getElementById('submitBlockMessage');
        let blockMessageTemplate = document.getElementById('template-submitBlockMessage').content.cloneNode(true);
        let alertMessage = blockMessageTemplate.querySelector('.alert');

        let textNode;
        if (message['type'] == 'success') {
            textNode = document.createTextNode(message['msg']);
            alertMessage.classList.add('alert-success');
        } else {
            textNode = document.createTextNode(message['msg']);
            alertMessage.classList.add('alert-danger');
        }

        alertMessage.appendChild(textNode);
        blockMessageContainer.prepend(
            document.importNode(blockMessageTemplate, true)
        );
    }

});
