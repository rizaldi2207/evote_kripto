import sqlite3
import binascii
import json
import asyncio
from typing import Tuple, Dict
from datetime import datetime
from flask import Flask, jsonify, render_template, request, redirect, url_for

from const import TEMPLATE_DIR, DB_DIR
from database import (count_candidate_votes, create_dht, select_dht)
from net import broadcast_toall
import holochain, crypto, threading

Block = Tuple[float, str, str]

app = Flask(__name__)
conn = cursor = None
def openDB():
    global conn, cursor
    conn = sqlite3.connect(DB_DIR)

def closeDB():
    global conn, cursor
    cursor.close()
    conn.close

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/contact', methods=['GET', 'POST'])
def contact():
    return render_template('contact.html')

@app.route('/data', methods=['GET', 'POST'])
async def add_dht():
    openDB()
    data = request.json

    candidates = { int(id): int(votes) \
                   for id, votes in data['candidates'].items() }
    cand_votes = tuple([ votes for id, votes in data['candidates'].items() ])


    private_key = data['private_key']
    timestamp = datetime.now()

    try:
        node_id = crypto.get_public_key_ecc(private_key)
        signature = holochain.sign_votes(private_key, node_id, candidates)
        block_data = (timestamp, node_id, signature)

        _ = await create_dht(conn, block_data, cand_votes)
        conn.commit()

    except sqlite3.IntegrityError:
        conn.rollback()
        msg_type, msg_text = 'error', 'You have entered data'

    except (binascii.Error, ValueError):
        conn.rollback()
        msg_type, msg_text = 'error', 'Wrong private key'
        msg = {
        'type': msg_type,
        'msg': msg_text
        }
        return jsonify(msg)
    except Exception:
        conn.rollback()
        msg_type, msg_text = 'error', 'Something went wrong'

    else:
        msg_type = 'success'
        msg_text = 'Data has been submitted successfully'

        Addr = [('192.168.43.48', 8000), ('192.168.43.172', 8000)]
        await broadcast_toall(Addr, (block_data+(candidates,)))

    msg = {
        'type': msg_type,
        'msg': msg_text
    }
    return jsonify(msg)

@app.route('/vote', methods=['GET', 'POST'])
async def vote():
    openDB()
    votes_count = await count_candidate_votes(conn)
    return jsonify(votes_count)

@app.route('/receive', methods=['GET', 'POST'])
async def receive():
    openDB()
    data = await select_dht(conn)
    return jsonify(data)

if __name__ == '__main__':
    app.run(debug=True, host='192.168.1.35')

