import asyncio
import json
import sqlite3
import holochain
from const import TEMPLATE_DIR, DB_DIR
from typing import Dict
from database import (count_candidate_votes, create_dht, select_dht)
from datetime import datetime

conn = cursor = None
def openDB():
    global conn, cursor
    conn = sqlite3.connect(DB_DIR)

def closeDB():
    global conn, cursor
    cursor.close()
    conn.close

async def handle_echo(reader:asyncio.StreamReader, writer:asyncio.StreamWriter):
    addr = writer.get_extra_info('peername')
    try:    
        data = await reader.readline()
        data_dict = json.loads(data.decode())

        await save_data(data_dict)
        
        print(f"received {data_dict!r} from {addr!r}")

    except Exception:
        raise
    writer.close()

async def run_server():
    server = await asyncio.start_server(handle_echo, '192.168.1.35', 8000)
    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    print(f'Serving on {addrs}')
    async with server:
        await server.serve_forever()

async def save_data(data: Dict[str, any]):
    openDB()
    candidates = { int(id): int(votes) \
                   for id, votes in data['candidates'].items() }
    cand_votes = tuple([ votes for id, votes in data['candidates'].items() ])
    block_data = ((str(datetime.fromtimestamp(data['timestamp']))), data['node_id'], data['signature'])
    verif_data = (data['node_id'], data['signature'], data['candidates'])

    is_data_valid = holochain.validasi_data(verif_data)

    if is_data_valid:
        try:
            ##block_data = ((str(datetime.fromtimestamp(data['timestamp'])),) + block_data[1:])

            _ = await create_dht(conn, block_data, cand_votes)
            conn.commit()

        except sqlite3.IntegrityError:
            conn.rollback()
    else:
         print("Failed to save data")